#
# Projekt: IPK3
# Autor:   Vera Mullerova, xmulle17@stud.fit.vutbr.cz
# Datum:   24.4.2011
#

SERVER          = rdtserver
CLIENT          = rdtclient
SERVER_SOURCES  = rdtserver.c
CLIENT_SOURCES  = rdtclient.c

DEFINES         = 

CFLAGS         = -std=c99 -g

LIBRARIES       = #-llibrary_name

CC              = gcc
SERVER_OBJECTS  = $(SERVER_SOURCES:.c=.o)
CLIENT_OBJECTS  = $(CLIENT_SOURCES:.c=.o)
INCLUDES        = #-I.
LIBDIRS         = 
LDFLAGS         = $(LIBDIRS) $(LIBRARIES)

###########################################

.SUFFIXES: .c .o

.c.o:
		$(CC) $(CFLAGS) -c $<

###########################################

all:		$(SERVER) $(CLIENT)

rebuild:	clean all

$(SERVER):	$(SERVER_OBJECTS)
		$(CC) $(SERVER_OBJECTS) $(LDFLAGS) -o $@

$(CLIENT):	$(CLIENT_OBJECTS)
		$(CC) $(CLIENT_OBJECTS) $(LDFLAGS) -o $@

###########################################

clean:
	rm -fr core* *~ $(SERVER_OBJECTS) $(CLIENT_OBJECTS) $(SERVER) $(CLIENT) .make.state .sb