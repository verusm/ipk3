/*    
*   Projekt: IPK3                                                                        
*   Nazev: rdtserver.c                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 24.4.2011                                                          
*   Popis: Implementace serveru pro zretezeny prenos dat RDT.           
*/

//import potrebnych knihoven
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <getopt.h>

#include <signal.h>
#include <sys/time.h>


#include "udt.h"

#define MAXCHAR  80
#define MAXCHARS 120
#define MAXLINES 500
#define END 5


//globalni promenne
in_port_t source_port = 0;
in_port_t dest_port = 0;
in_addr_t dest_addr = 0x7f000001;
int udt;


/** 
 * Funkce pro zpracovani vstupnich parametru. 
 * @param argc Pocet argumentu.
 * @param **argv Argumenty.
 * @return Vraci pripadnou chybu.
 */
int GetParams(int argc, char **argv){
  if (argc < 3){
    fprintf(stderr, "Chybi povinny parametr zdrojovy port!\n");
    return -1;
  }
  else if (argc < 5){
    fprintf(stderr, "Chybi povinny parametr cilovy port!\n");
    return -1;
  }
  else if (argc != 5){
    fprintf(stderr, "Chybny pocet zadanych parametru!\n");
    return -1;
  }
  
  int ch = 0;
  // cyklus se provadi dokud jsou na vstupu nejake parametry zacinajici pomlckou
  while((ch = getopt(argc, argv, "s:d:h")) != -1){
    switch(ch){
      case 's':
	  source_port = atol(optarg);
	break;
      case 'd':
	  dest_port = atol(optarg);
	break;
      case 'h':
	  fprintf(stderr, "Napoveda k programu \n");
	break;
      default:
	  return -1;
	break;
    }
  }
  return 0; 
}

/** Vypocet kontrolniho souctu.
 * @param addr Adresa, od ktere zacinaji data pro vypocet.
 * @param count Pocet bytu, ze kterych se kontrolni soucet vypocita.
 */
int countChecksum(char *addr, int count){
  int checksum = 0;
  for (int i = 0; i < count; i++){
    checksum = checksum ^ addr[i];
  }
  return checksum;
}

/** Vytvoreni paketu pro odeslani.
 * @param nextseqnumber Cislo paketu.
 */
char *makePacket(int nextseqnumber){
  char *packet = NULL;
  char pack[MAXCHARS];
  char buffer[10];
  
  if((packet = malloc(MAXCHARS)) == NULL){	//alokace pameti pro paket
    fprintf(stderr, "Chyba pri alokaci pameti! \n");
    EXIT_FAILURE;
  }
  
  strcpy(packet, "\0");
  strcpy(pack, "\0");
  
  sprintf(buffer, "%d", nextseqnumber);
  strcat(pack, buffer);		//vlozeni cisla paketu do hlavicky
  strcat(pack, "\n");
  strcat(pack, "ACK");
  //vypocet kontrolniho souctu
  sprintf(buffer, "%d:", countChecksum(pack, strlen(pack)));
  strcat(packet, buffer);	//vlozeni kontrolniho souctu do hlavicky
  strcat(packet, pack);
  
  return packet;  
}

/** Ziskani dat z paketu.
 * @param recvpacket Prichozi paket.
 * @param recvline Prichozi data.
 */
void extractPacket(char *recvpacket, char *recvline){
  strcpy(recvline, strstr(recvpacket, "\n") + strlen("\n"));
  return;
}

/** Overeni kontrolniho souctu a cisla paketu.
 * @param recvpacket Prichozi paket.
 * @param lenght Delka prichoziho paketu.
 * @param seqnum Cislo prichoziho paketu.
 */
int verificationChecksum(char *recvpacket, int lenght, int *seqnum){
  int i = 0;
  int j = 0;
  char packet[MAXCHARS];
  char buffer[10];
  int checksumHead;
  int checksumPacket;
  
  //ziskani kontrolniho souctu z hlavicky paketu
  while(recvpacket[i] != ':'){
    buffer[i] = recvpacket[i];
    i++;
    if ((i >= lenght) || (i >= 9)){
      return 0;
    }
  }
  
  buffer[i] = "\0";
  if (strcmp(buffer, "END") == 0){
    return END;
  }
  checksumHead = atoi(buffer);
  
  strcpy(packet, recvpacket + strlen(buffer) + 1);
  checksumPacket = countChecksum(packet, strlen(packet));	//ziskani kontrolniho souctu paketu
  
  i++;
  //zjisteni cisla paketu
  while(recvpacket[i] != '\n'){
    buffer[j] = recvpacket[i];
    i++;
    j++;
    if ((i >= lenght) || (i >= 9)) {
      return 0;
    }
  }
  buffer[j] = "\0";
  (*seqnum) = atoi(buffer);
  
  if (checksumHead == checksumPacket){
    return 1;
  }
  
  return 0; 
}

/** UDTconnection
 * 
 */
int UDTconnection(){  
  char recvpacket[MAXCHARS];
  int expectedseqnum = 1;
  int seqnum;
  int length;
  int result;
  char *sendpacket;
  char *recvline;
  
  if((sendpacket = malloc(MAXCHARS)) == NULL){
    fprintf(stderr, "Chyba pri alokaci pameti! \n");
    return -1;
  }
  if((recvline = malloc(MAXCHARS)) == NULL){
    fprintf(stderr, "Chyba pri alokaci pameti! \n");
    return -1;
  } 
  
  // inicializace udt spojeni
  udt = udt_init(source_port);
  
  fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);
  
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(udt, &readfds);
  FD_SET(STDIN_FILENO, &readfds);
  
  while(select(udt + 1, &readfds, NULL, NULL, NULL)){	//cekani serveru
    // cteni dat
    if(FD_ISSET(udt, &readfds)){
      length = udt_recv(udt, recvline, MAXCHARS, NULL, NULL);	//prijem paketu
      // zjisteni kontrolniho souctu
      result = verificationChecksum(recvpacket, length, &seqnum);
      // pokud vraci END, tak jsme na konci prenasenych dat a ukoncime server
      if(result == END){
        free(sendpacket);
	free(recvline);
	return 0;
      }
      // kontrolni soucet a cislo paketu jsou v poradku, ale jeste nejsem na konci
      if((result == 1) && (seqnum == expectedseqnum)){
	extractPacket(recvpacket, recvline);	//ziskani dat z paketu
        fputs(recvline, stdout);	// vypis dat na standardni vystup
	sendpacket = makePacket(expectedseqnum);	//vytvoreni ACK paketu
	//odeslani paketu klientovi
        if(!udt_send(udt, dest_addr, dest_port, sendpacket, strlen(sendpacket) + 1)){
	  fprintf(stderr, "Chyba pri odesilani paketu! \n");
	  return -1;
        }
        expectedseqnum++;
      }
      else{	// chyba v kontrolnim souctu nebo v cisle paketu
        if(sendpacket != NULL){
	  udt_send(udt, dest_addr, dest_port, sendpacket, strlen(sendpacket) + 1);	//odeslani paketu
	}
      }
    }

    FD_ZERO(&readfds);
    FD_SET(udt, &readfds);
    FD_SET(STDIN_FILENO, &readfds);
  }      
  return 0;
}

/** Hlavni program */  
int main(int argc, char **argv){
  // kontrola a zpracovani povinnych parametru a jejich poctu
  if (GetParams(argc, argv) != 0){
    EXIT_FAILURE;
  }
  if (UDTconnection() != 0){
    EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
}
