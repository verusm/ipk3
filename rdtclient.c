/*    
*   Projekt: IPK3                                                                        
*   Nazev: rdtclient.c                                                    
*   Autor: Vera Mullerova, xmulle17@stud.fit.vutbr.cz                                                                 
*   Datum: 24.4.2011                                                          
*   Popis: Implementace klienta pro zretezeny prenos dat RDT.           
*/

//import potrebnych knihoven
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdarg.h>
#include <getopt.h>

#include <signal.h>
#include <sys/time.h>


#include "udt.h"

#define MAXCHAR  80
#define MAXCHARS 120
#define MAXLINES 500
//maximalni zpozdeni na lince 500ms
#define TIMEOUT 0.5
#define WINDOWSIZE 5

//globalni promenne
in_port_t source_port = 0;
in_port_t dest_port = 0;
in_addr_t dest_addr = 0x7f000001;
int udt;
struct itimerval itv;
sigset_t sigmask;
int send_base = 1;
int ackn = 1;
int nextseqnum = 1;
char **sendpacket = NULL;
char *bufferLines[MAXCHARS];

/** 
 * Funkce pro zpracovani vstupnich parametru. 
 * @param argc Pocet argumentu.
 * @param **argv Argumenty.
 * @return Vraci pripadnou chybu.
 */
int GetParams(int argc, char **argv){
  if (argc < 3){
    fprintf(stderr, "Chybi povinny parametr zdrojovy port!\n");
    return -1;
  }
  else if (argc < 5){
    fprintf(stderr, "Chybi povinny parametr cilovy port!\n");
    return -1;
  }
  else if (argc != 5){
    fprintf(stderr, "Chybny pocet zadanych parametru!\n");
    return -1;
  }
  
  int ch = 0;
  // cyklus se provadi dokud jsou na vstupu nejake parametry zacinajici pomlckou
  while((ch = getopt(argc, argv, "s:d:h")) != -1){
    switch(ch){
      case 's':
	  source_port = atol(optarg);
	break;
      case 'd':
	  dest_port = atol(optarg);
	break;
      case 'h':
	  fprintf(stderr, "Napoveda k programu \n");
      default:
	  return -1;
	break;
    }
  }
  return 0; 
}

/** Obsluha programu pri vyprseni casu.
 * @param sig Prichozi signal
 */
void sigalrm_handler(int sig) {	
  //spusteni casovace
  setitimer(ITIMER_REAL, &itv, NULL);
  //sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
  
  for (int i = send_base; i <= nextseqnum-1; i++){
    //odeslani paketu
    udt_send(udt, dest_addr, dest_port, sendpacket[i % WINDOWSIZE], strlen(sendpacket[i % WINDOWSIZE]) + 1);
  }
  
  //reinstalace handleru
  signal(SIGALRM, sigalrm_handler);
  return;
}

/** Vypocet kontrolniho souctu.
 * @param addr Adresa, kde zacinaji data pro vypocet.
 * @param count Pocet bytu, ze kterych se pocita kontrolni soucet.
 */
int countChecksum(char *addr, int count){  
  int checksum = 0;
  for (int i = 0; i < count; i++){
    checksum = checksum ^ addr[i];
    i++;
  }
  return checksum;
}

/** Overeni kontrolniho souctu.
 * 
 */
int verificationChecksum(char *recvpacket, int length, int *seqnum){
  int i = 0;
  int j = 0;
  char packet[MAXCHARS];
  char buffer[10];
  int checksumHead;
  int checksumPacket;
  
  //ziskani kontrolniho souctu z hlavicky paketu
  while(recvpacket[i] != ':'){
    buffer[i] = recvpacket[i];
    i++;
    if ((i >= length) || (i >= 9)){
      return 0;
    }
  }
  
  buffer[i] = "\0";
  checksumHead = atoi(buffer);
  
  strcpy(packet, recvpacket + strlen(buffer) + 1);
  checksumPacket = countChecksum(packet, strlen(packet));	//ziskani kontrolniho souctu paketu
  
  i++;
  //zjisteni cisla paketu
  while(recvpacket[i] != '\n'){
    buffer[j] = recvpacket[i];
    i++;
    j++;
    if ((i >= length) || (i >= 9)) {
      return 0;
    }
  }
  buffer[j] = "\0";
  (*seqnum) = atoi(buffer);
  
  if (checksumHead == checksumPacket){
    return 1;
  }
  
  return 0; 
}

/** Vytvoreni paketu pro odeslani.
 * 
 */
char* makePacket(int nextseqnumber, char *sendline){
  char *packet;
  char pack[MAXCHARS];
  char buffer[10];
  
  if((packet = malloc(MAXCHARS)) == NULL){	//alokace pameti pro paket
    fprintf(stderr, "Chyba pri alokaci pameti! \n");
    EXIT_FAILURE;
  }
  
  strcpy(packet, "\0");
  strcpy(pack, "\0");
  
  sprintf(buffer, "%d", nextseqnumber);
  strcat(pack, buffer);		//vlozeni cisla paketu do hlavicky
  strcat(pack, "\n");
  strcat(pack, sendline);	//vlozeni dat
  //vypocet kontrolniho souctu
  sprintf(buffer, "%d:", countChecksum(pack, strlen(pack)));
  strcat(packet, buffer);	//vlozeni kontrolniho souctu do hlavicky
  strcat(packet, pack);
  
  return packet;
}

/** UDTconnection
 *
 */
int UDTconnection(){
  char recvpacket[MAXLINES];
  char sendline[MAXCHAR];
  int length;
  int seqnum;
  int num = 0;
  
  if((sendpacket = malloc(WINDOWSIZE)) == NULL){
    fprintf(stderr, "Chyba pri alokaci pameti! \n");
    return -1;
  }
  
  //inicializace udt
  udt = udt_init(source_port);
  fcntl(STDIN_FILENO, F_SETFL, O_NONBLOCK);	// make stdin reading non-clocking
  
  fd_set readfds;
  FD_ZERO(&readfds);
  FD_SET(udt, &readfds);
  FD_SET(STDIN_FILENO, &readfds);
  
  while (select(udt+1, &readfds, NULL, NULL, NULL)) {	//cekani klienta
    //odeslani dat
    if(FD_ISSET(STDIN_FILENO, &readfds) && fgets(sendline, MAXCHAR, stdin) > 0){
      if(nextseqnum < send_base + WINDOWSIZE){	//pokud je slide window volny, tak se odesle paket
        sendpacket[nextseqnum % WINDOWSIZE] = makePacket(nextseqnum, sendline);  // vytvoreni packetu
	//odeslani paketu
        if(!udt_send(udt, dest_addr, dest_port, sendpacket[nextseqnum % WINDOWSIZE], strlen(sendpacket[nextseqnum % WINDOWSIZE]) + 1)){
	  fprintf(stderr, "Chyba pri odesilani paketu!\n");
	  return -1;
	}
	
        if(send_base == nextseqnum){		//kontrola, zda jsou odeslana vsechna data
	  //nastaveni casovace
	  setitimer(ITIMER_REAL, &itv, NULL);
	  //sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
	}
        nextseqnum++;
      }
      //pokud je slide window plny, tak se ulozi odkaz na radek
      else{
	bufferLines[num] = sendline;
	num++;
      }
    }
    
    //prijem data
    if(FD_ISSET(udt, &readfds)){
      length = udt_recv(udt, recvpacket, MAXLINES, NULL, NULL);		//prijem paketu
      //kontrola kontrolniho souctu
      if (verificationChecksum(recvpacket, length, &seqnum)){
	send_base = seqnum + 1;
	//pokud jsou prijata vsechna data, zasle se paket oznamujici konec
	if(send_base == nextseqnum){
	  //sigprocmask(SIG_BLOCK, &sigmask, NULL);
	  char packet[MAXCHARS];
	  strcat(packet, "end \n");
	  if(!udt_send(udt, dest_addr, dest_port, packet, strlen(packet) + 1)){
	    fprintf(stderr, "Chyba pri odesilani dat! \n");
	    return -1;
	  }
	}
	else {
	  //nastaveni casovace
	  setitimer(ITIMER_REAL, &itv, NULL);
	  //sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
	}
	//pokud je treba jeste neco odeslat a je volne slide window, tak se to zasle
	/*
	if (nextseqnum < send_base + WINDOWSIZE){
	  if (bufferLines[0] != NULL){
	    ackn = send_base;
	    sendline = *bufferLines[0];
	    sendpacket = makePacket(nextseqnum, sendline);
	    if(!udt_send(udt, dest_addr, dest_port, sendpacket, strlen(sendpacket) + 1)){
	      fprintf(stderr, "Chyba pri odesilani dat! \n");
	      return -1;
	    }
	    if (send_base == nextseqnum){
	      //nastaveni casovace
	      setitimer(ITIMER_REAL, &itv, NULL);
	      //sigprocmask(SIG_UNBLOCK, &sigmask, NULL);
	    }
	    nextseqnum++;
	  }
	}
	*/
      }
    }    
    
    FD_ZERO(&readfds);
    FD_SET(udt, &readfds);
    FD_SET(STDIN_FILENO, &readfds);
  }  
  return 0;
}

/** Hlavni program */  
int main(int argc, char **argv){
  // kontrola a zpracovani povinnych parametru a jejich poctu
  if (GetParams(argc, argv) != 0){
    EXIT_FAILURE;
  }
  
  //nastaveni casovace
  signal(SIGALRM, sigalrm_handler);
  itv.it_interval.tv_sec = TIMEOUT;
  itv.it_interval.tv_usec = 0;
  itv.it_value.tv_sec = TIMEOUT;
  itv.it_value.tv_usec = 0;
 
  if (UDTconnection() != 0){
    EXIT_FAILURE;
  }
  return EXIT_SUCCESS;
} // konec funkce main()